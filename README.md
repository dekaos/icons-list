 **[Evil Icons](http://evil-icons.io/)** 70 icons  
 **[Ant Design Icons](https://ant.design/components/icon/)** 297 icons    
 **[Entypo](http://entypo.com/)** 411 icons     
 **[Feather](https://feathericons.com/)** 266 icons  
 **[Font Awesome](http://fortawesome.github.io/Font-Awesome/icons/)** 675 icons  
 **[Foundation](http://zurb.com/playground/foundation-icon-fonts-3)** 283 icons    
 **[Font Awesome 5](https://fontawesome.com/)** 1409 free      
 **[Ionicons](https://ionicons.com/)** 696 icons  
 **[Material Community Icons](https://materialdesignicons.com/)** 2894 icons  
 **[Material Icons](https://www.google.com/design/icons/)** 932 icons  
 **[Octicons](http://octicons.github.com/)** 177 icons  
 **[Zocial](http://zocial.smcllns.com/)** 100 icons  
 **[Simple Line Icons](https://github.com/thesabbir/simple-line-icons)** 189 icons  
 
  
 **[https://instabug.com/blog/react-native-ui-libraries](https://instabug.com/blog/react-native-ui-libraries)**             